#!/usr/bin/env bash

set -u

if [[ -d "package/${1}" ]]; then
    rm -rf "package/${1}"
    rm release/"${1}"-*.pkg.tar.zst
else
    echo "Cannot remove directory '${1}': File does not exist"
fi
